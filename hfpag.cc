#include <QtCore/QDebug>
#include "hfpag.h"

HfpAg::HfpAg(const QBluetoothAddress &addr, QObject *parent) :
    QObject(parent), _socket(new QBluetoothSocket(QBluetoothSocket::RfcommSocket, this))
{
	connect(_socket, SIGNAL(connected()), SLOT(handleConnected()));
	connect(_socket, SIGNAL(disconnected()), SLOT(handleDisconnected()));
	connect(_socket, SIGNAL(readyRead()), SLOT(handleReadyRead()));
	qDebug() << "Starting HFP connection to " << addr.toString();
	_socket->connectToService(addr, 7); // HFP AG
}

HfpAg::~HfpAg()
{
	qDebug() << "Destroying HFP";
}

void HfpAg::send(const QString &cmd)
{
	_socket->write("\r\n", 2);
	_socket->write(cmd.toLatin1());
	_socket->write("\r\n", 2);
	//qDebug() << "HFP response:" << cmd;
}

void HfpAg::handleConnected()
{
	qDebug() << "Connected to HFP";
}

void HfpAg::handleDisconnected()
{
	qDebug() << "Disconnected from HFP";
}

void HfpAg::handleReadyRead()
{
	_inBuf.append(_socket->readAll());

	int offset = _inBuf.indexOf("\r\n");
	while (offset >= 0) {
		QString cmd = QString::fromLatin1(_inBuf.mid(0, offset));
		_inBuf.remove(0, offset + 2);

		handleCommand(cmd);

		offset = _inBuf.indexOf("\r\n");
	}
}

void HfpAg::handleCommand(const QString &cmd)
{
	//qDebug() << "HFP command:" << cmd;

	if (cmd.startsWith("AT+BRSF=")) {
		send("+BRSF: 20");
		send("OK");
	} else if (cmd == "AT+CIND=?") {
		send("+CIND: (\"call\",(0,1)),(\"callsetup\",(0-3)),(\"service\",(0-1)),(\"signal\",(0-5)),(\"roam\",(0,1)),(\"battchg\",(0-5)),(\"callheld\",(0-2))");
		send("OK");
	} else if (cmd == "AT+CIND?") {
		send("+CIND: 0,0,0,0,0,3,0");
		send("OK");
	} else if (cmd.startsWith("AT+CMER=")) {
		send("OK");
	} else if (cmd.startsWith("AT+CLIP=")) {
		send("OK");
	} else if (cmd.startsWith("AT+COPS=")) {
		send("OK");
	} else if (cmd.startsWith("AT+CCWA=")) {
		send("OK");
	} else if (cmd.startsWith("AT+BIA=")) {
		send("OK");
	} else if (cmd.startsWith("AT+CLCC=")) {
		send("OK");
	} else if (cmd.startsWith("ATD")) {
		qDebug() << "Dialing" << cmd.mid(3);
		send("OK");
	} else {
		qWarning() << "Unknown AT command: " << cmd;
		send("+CME ERROR: 0");
	}
}
