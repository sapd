#include <iostream>

#include <QtCore/QCoreApplication>
#include <QtCore/QSettings>
#include <QtCore/QStringList>

#include <sap/sapmanager.h>
#include <sap/sapbtlistener.h>
#include <sap/capabilityagent.h>

#include "agents/hostmanageragent.h"
#include "agents/webproxyagent.h"
#include "agents/notificationagent.h"
#include "agents/musicagent.h"

using std::cerr;
using std::endl;

int main(int argc, char *argv[])
{
	QCoreApplication app(argc, argv);
	app.setOrganizationName("javispedro");
	app.setOrganizationDomain("com.javispedro");
	app.setApplicationName("sapd");

	QBluetoothAddress address;
	QSettings settings;
	QString address_from_settings = settings.value("device/address").toString();
	if (!address_from_settings.isEmpty()) {
		address = QBluetoothAddress(address_from_settings);
	}

	if (app.arguments().size() >= 2) {
		// Get the address from the command line.
		address = QBluetoothAddress(app.arguments().at(1));
	}

	if (address.isNull()) {
		cerr << "Usage: sapd <bt address>" << endl;
		cerr << "If you don't want to specify the BT address every time, put it in "
			 << qPrintable(settings.fileName())
			 << endl;
		settings.setValue("device/address", QString());
		return EXIT_FAILURE;
	}

	SAPManager *manager = SAPManager::instance();

	manager->registerApplicationPackage("com.samsung.accessory.goproviders");
	manager->registerApplicationPackage("com.samsung.accessory.saproviders", "saproviders", 62);
	manager->registerApplicationPackage("com.sec.android.weatherprovider");
	manager->registerApplicationPackage("com.sec.android.fotaprovider");

	CapabilityAgent::registerServices(manager);
	HostManagerAgent::registerServices(manager);
	WebProxyAgent::registerServices(manager);
	NotificationAgent::registerServices(manager);
	MusicAgent::registerServices(manager);

	QScopedPointer<SAPBTListener> sap_listener(new SAPBTListener);

	sap_listener->start();
	sap_listener->nudge(address);

	return app.exec();
}
