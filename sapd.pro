TARGET = sapd
TEMPLATE = app
QT       += core gui dbus network bluetooth
CONFIG   += console c++11

CONFIG    += link_pkgconfig
PKGCONFIG += openssl

exists(/usr/lib/pkgconfig/sailfishapp.pc) {
    # Building for Jolla Sailfish, Qt5, Bluez4(?)
    DEFINES  += SAILFISH
    PKGCONFIG += bluez-qt5
    WATCHFISH_FEATURES = notificationmonitor walltime music
    include(libwatchfish/libwatchfish.pri)
} else {
    # Building for desktop, Qt5, Bluez5
    DEFINES  += DESKTOP
    SOURCES  += hfpag.cc
    HEADERS  += hfpag.h
}

target.path = /usr/bin
INSTALLS += target

SOURCES += main.cc \
    sap/sapbtlistener.cc \
    sap/wmspeer.cc \
    sap/sapbtpeer.cc \
    sap/saprotocol.cc \
    sap/keys/Tout.c \
    sap/keys/Tin.c \
    sap/keys/T2.c \
    sap/keys/T1.c \
    sap/keys/T0.c \
    sap/keys/psk_table.c \
    sap/keys/finalT1.c \
    sap/keys/finalT0.c \
    sap/keys/DecTout.c \
    sap/keys/DecTin.c \
    sap/keys/DecT2.c \
    sap/keys/DecT1.c \
    sap/keys/DecT0.c \
    sap/wmscrypt.cc \
    sap/crc16.cc \
    sap/sapsocket.cc \
    sap/sappeer.cc \
    sap/capabilityagent.cc \
    sap/sapmanager.cc \
    sap/sapserviceinfo.cc \
    sap/sapchannelinfo.cc \
    sap/sapconnection.cc \
    sap/sapconnectionrequest.cc \
    sap/capabilitypeer.cc \
    agents/hostmanageragent.cc \
    agents/hostmanagerconn.cc \
    agents/notificationagent.cc \
    agents/notificationconn.cc \
    agents/webproxyagent.cc \
    agents/webproxyconn.cc \
    agents/webproxytrans.cc \
    agents/musicagent.cc \
    agents/musicconn.cc

HEADERS += \
    sap/sapbtlistener.h \
    sap/wmspeer.h \
    sap/sapbtpeer.h \
    sap/saprotocol.h \
    sap/wmskeys.h \
    sap/wmscrypt.h \
    sap/crc16.h \
    sap/endianhelpers.h \
    sap/sapsocket.h \
    sap/sapagent.h \
    sap/sappeer.h \
    sap/capabilityagent.h \
    sap/sapmanager.h \
    sap/sapserviceinfo.h \
    sap/sapchannelinfo.h \
    sap/sapconnection.h \
    sap/sapconnectionrequest.h \
    sap/capabilitypeer.h \
    agents/hostmanageragent.h \
    agents/hostmanagerconn.h \
    agents/notificationagent.h \
    agents/notificationconn.h \
    agents/webproxyagent.h \
    agents/webproxyconn.h \
    agents/webproxytrans.h \
    agents/musicagent.h \
    agents/musicconn.h

OTHER_FILES += \
    rpm/sapd.yaml \
    sailfish/org.freedesktop.Notifications.xml
