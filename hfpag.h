#ifndef HFPAG_H
#define HFPAG_H

#include <QtCore/QObject>
#include <QtBluetooth/QBluetoothSocket>

class HfpAg : public QObject
{
	Q_OBJECT
public:
	explicit HfpAg(const QBluetoothAddress &addr, QObject *parent = 0);
	~HfpAg();

private:
	void send(const QString& cmd);

private slots:
	void handleConnected();
	void handleDisconnected();
	void handleReadyRead();
	void handleCommand(const QString &cmd);

private:
	QBluetoothSocket *_socket;
	QByteArray _inBuf;
};

#endif // HFPAG_H
