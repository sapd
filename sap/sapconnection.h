#ifndef SAPCONNECTION_H
#define SAPCONNECTION_H

#include <QtCore/QObject>
#include <QtCore/QMap>

class SAPPeer;
class SAPSocket;

class SAPConnection : public QObject
{
	Q_OBJECT

	SAPConnection(SAPPeer *peer, const QString &profile);

public:
	SAPPeer *peer();

	QString profile() const;

	SAPSocket *getSocket(int channelId);

	QList<SAPSocket*> sockets();

signals:
	void connected();
	void error(int errorCode);
	void disconnected();

private:
	void setSocket(int channelId, SAPSocket *socket);

private:
	const QString _profile;
	QMap<int, SAPSocket*> _sockets;

	friend class SAPPeer;
};

#endif // SAPCONNECTION_H
