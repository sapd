#include "sapchannelinfo.h"
#include <QSharedData>

struct SAPChannelInfoData : public QSharedData {
	unsigned short id;
	SAPChannelInfo::PayloadType payload;
	SAPChannelInfo::QoSType qosType;
	SAPChannelInfo::QoSPriority qosPriority;
	SAPChannelInfo::QoSDataRate qosDataRate;
};

SAPChannelInfo::SAPChannelInfo() : data(new SAPChannelInfoData)
{
}

SAPChannelInfo::SAPChannelInfo(const SAPChannelInfo &rhs) : data(rhs.data)
{
}

SAPChannelInfo &SAPChannelInfo::operator=(const SAPChannelInfo &rhs)
{
	if (this != &rhs)
		data.operator=(rhs.data);
	return *this;
}

SAPChannelInfo::~SAPChannelInfo()
{

}

unsigned short SAPChannelInfo::channelId() const
{
	return data->id;
}

void SAPChannelInfo::setChannelId(unsigned short id)
{
	data->id = id;
}

SAPChannelInfo::PayloadType SAPChannelInfo::payloadType() const
{
	return data->payload;
}

void SAPChannelInfo::setPayloadType(PayloadType type)
{
	data->payload = type;
}

SAPChannelInfo::QoSType SAPChannelInfo::qosType() const
{
	return data->qosType;
}

void SAPChannelInfo::setQoSType(QoSType type)
{
	data->qosType = type;
}

SAPChannelInfo::QoSPriority SAPChannelInfo::qosPriority() const
{
	return data->qosPriority;
}

void SAPChannelInfo::setQoSPriority(QoSPriority priority)
{
	data->qosPriority = priority;
}

SAPChannelInfo::QoSDataRate SAPChannelInfo::qosDataRate() const
{
	return data->qosDataRate;
}

void SAPChannelInfo::setQoSDataRate(QoSDataRate rate)
{
	data->qosDataRate = rate;
}

QDebug operator<<(QDebug debug, const SAPChannelInfo &info)
{
	QDebugStateSaver saver(debug);
	Q_UNUSED(saver);
	debug.nospace() << "SAPChannelInfo(" << info.channelId() << ", qosType=" << info.qosType() << ", dataRate=" << info.qosDataRate() << ", priority=" << info.qosPriority()
					<< ", payload=" << info.payloadType() << ")";
	return debug;
}
