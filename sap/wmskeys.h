#ifndef WMSKEYS_H
#define WMSKEYS_H

extern unsigned char key_DecT0[];
extern unsigned char key_DecT1[];
extern unsigned char key_DecT2[];
extern unsigned char key_DecTin[];
extern unsigned char key_DecTout[];

extern unsigned char key_finalT0[];
extern unsigned char key_finalT1[];

extern unsigned char key_psk_table[];

extern unsigned char key_T0[];
extern unsigned char key_T1[];
extern unsigned char key_T2[];
extern unsigned char key_Tin[];
extern unsigned char key_Tout[];

#endif // WMSKEYS_H
