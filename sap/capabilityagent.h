#ifndef CAPABILITYAGENT_H
#define CAPABILITYAGENT_H

#include <QtCore/QObject>
#include <QtCore/QPointer>
#include <QtCore/QHash>

#include "sapagent.h"

class SAPManager;

class CapabilityAgent : public QObject, public SAPAgent
{
	Q_OBJECT

	explicit CapabilityAgent(QObject *object = 0);

public:
	static CapabilityAgent* instance();
	static void registerServices(SAPManager *manager);

	void peerFound(SAPPeer *peer);
	void requestConnection(SAPConnectionRequest *request);
};

#endif // CAPABILITYAGENT_H
