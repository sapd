#ifndef SAPPEER_H
#define SAPPEER_H

#include <QtCore/QObject>
#include <QtCore/QQueue>

#include "saprotocol.h"
#include "sapserviceinfo.h"

class WMSPeer;
class CapabilityPeer;
class SAPConnection;
class SAPConnectionRequest;
class SAPSocket;
class SAPAgent;

class SAPPeer : public QObject
{
	Q_OBJECT
public:
	explicit SAPPeer(SAProtocol::Role role, const QString &localName, const QString &peerName, QObject *parent = 0);
	~SAPPeer();

	SAPConnection* createServiceConnection(const QString &profile, const QString &requesterProfile, SAPServiceInfo::Role requesterRole);
	bool terminateServiceConnection(const QString &profile, const QString &requesterProfile, SAPServiceInfo::Role requesterRole);

	SAProtocol::Role role() const;
	QString localName() const;
	QString peerName() const;

signals:
	void connected();
	void disconnected();

protected:
	/** Transmits a frame to the underlying transport. */
	virtual void sendFrame(const QByteArray &data) = 0;

	CapabilityPeer *capabilityPeer();

	/** Writes data to the remote. */
	void writeDataToSession(int session, const QByteArray &data);
	/** Writes a control frame to the remote. */
	void writeControlToSession(int session, const QByteArray &control);

	void acceptServiceConnection(SAPConnectionRequest *connReq, int statusCode);

	/** Distributes data to the appropiate socket. */
	void handleSessionData(int session, const QByteArray &data);

	/** Distributes a control message to the apropiate socket. */
	void handleSessionControl(int session, const QByteArray &data);

	/** Perform service discovery once connected. */
	void handleConnected();
	void handleDisconnected();

private:
	int findUnusedSessionId() const;
	void handleDefaultSessionMessage(const QByteArray &message);

protected:
	WMSPeer *_wms;

private:
	const SAProtocol::Role _role;
	const QString _localName;
	const QString _peerName;

	QMap<QString, SAPConnection*> _conns;
	QMap<int, SAPSocket*> _sessions;

	friend class SAPSocket;
	friend class SAPConnectionRequest;
};

#endif // SAPPEER_H
