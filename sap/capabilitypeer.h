#ifndef CAPABILITYPEER_H
#define CAPABILITYPEER_H

#include <QtCore/QObject>
#include <QtCore/QHash>
#include <QtCore/QMap>

#include "sapserviceinfo.h"

class SAPPeer;
class SAPConnection;
class SAPConnectionRequest;
class SAPSocket;

class CapabilityPeer : public QObject
{
	Q_OBJECT
public:
	CapabilityPeer(SAPPeer *peer, QObject *parent = 0);

	void requestConnection(SAPConnectionRequest *request);

	int remoteAgentId(const QString &profile, SAPServiceInfo::Role role);
	SAPServiceInfo remoteServiceInfo(int agentId) const;

private:
	QHash<QString, int>* profilesByRole(SAPServiceInfo::Role role);

private slots:
	void handleConnected();
	void handleMessageReceived();

private:
	SAPPeer *_peer;
	SAPConnection *_conn;
	SAPSocket *_socket;

	struct RemoteAgent {
		int agentId;
		SAPServiceInfo info;
	};

	QMap<int, RemoteAgent> _remoteAgents;

	QHash<QString, int> _consumerProfiles;
	QHash<QString, int> _providerProfiles;
};

#endif // CAPABILITYPEER_H
