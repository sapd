#ifndef SAPCONNECTIONREQUEST_H
#define SAPCONNECTIONREQUEST_H

#include <QtCore/QList>

class SAPPeer;
class SAPConnection;

class SAPConnectionRequest
{
	SAPConnectionRequest(SAPConnection *conn, int initiatorId, int acceptorId);

public:
	SAPPeer * peer();
	SAPConnection *connection();

	void accept();
	void reject(int reason = 1);

protected:
	int initiatorId() const;
	int acceptorId() const;

private:
	SAPConnection *_conn;
	int _initiator;
	int _acceptor;

	friend class SAPPeer;
};

#endif // SAPCONNECTIONREQUEST_H
