#include "sapchannelinfo.h"
#include "sapmanager.h"
#include "sappeer.h"
#include "sapsocket.h"
#include "sapserviceinfo.h"
#include "sapconnectionrequest.h"
#include "capabilitypeer.h"
#include "capabilityagent.h"

static CapabilityAgent *agent = 0;

CapabilityAgent::CapabilityAgent(QObject *parent)
    : QObject(parent)
{
}

CapabilityAgent *CapabilityAgent::instance()
{
	if (!agent) {
		agent = new CapabilityAgent;
	}

	return agent;
}

void CapabilityAgent::registerServices(SAPManager *manager)
{
	CapabilityAgent *agent = instance();
	SAPServiceInfo service;
	SAPChannelInfo channel;

	service.setProfile(SAProtocol::capabilityDiscoveryProfile);
	service.setFriendlyName("CapabilityAgentProvider");
	service.setRole(SAPServiceInfo::RoleProvider);
	service.setVersion(1);
	service.setConnectionTimeout(0);

	channel.setChannelId(SAProtocol::capabilityDiscoveryChannel);
	channel.setPayloadType(SAPChannelInfo::PayloadBinary);
	channel.setQoSType(SAPChannelInfo::QoSRestricted);
	channel.setQoSDataRate(SAPChannelInfo::QoSDataRateHigh);
	channel.setQoSPriority(SAPChannelInfo::QoSPriorityHigh);
	service.addChannel(channel);

	manager->registerServiceAgent(service, agent);

	service.setFriendlyName("CapabilityAgentConsumer");
	service.setRole(SAPServiceInfo::RoleConsumer);

	manager->registerServiceAgent(service, agent);
}

void CapabilityAgent::peerFound(SAPPeer *peer)
{
	// We make the capability peer a child of the peer object itself,
	// so that the peer can find it.
	CapabilityPeer *capPeer = new CapabilityPeer(peer, peer);
	connect(peer, SIGNAL(disconnected()), capPeer, SLOT(deleteLater()));
}

void CapabilityAgent::requestConnection(SAPConnectionRequest *request)
{
	SAPPeer *peer = request->peer();
	CapabilityPeer *capPeer = peer->findChild<CapabilityPeer*>();

	Q_ASSERT(capPeer);

	capPeer->requestConnection(request);
}
