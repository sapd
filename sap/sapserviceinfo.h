#ifndef SAPSERVICEINFO_H
#define SAPSERVICEINFO_H

#include <QtCore/QObject>
#include <QtCore/QSharedDataPointer>

class SAPServiceInfoData;
class SAPChannelInfo;

class SAPServiceInfo
{
	Q_GADGET
	Q_ENUMS(Role)

public:
	SAPServiceInfo();
	SAPServiceInfo(const SAPServiceInfo &);
	SAPServiceInfo &operator=(const SAPServiceInfo &);
	~SAPServiceInfo();

	enum Role {
		RoleProvider,
		RoleConsumer
	};

	static Role oppositeRole(Role role);

	QString profile() const;
	void setProfile(const QString &profile);

	QString friendlyName() const;
	void setFriendlyName(const QString &name);

	Role role() const;
	void setRole(Role role);

	unsigned short version() const;
	void setVersion(unsigned short version);
	void setVersion(unsigned char maj, unsigned char min);

	unsigned short connectionTimeout() const;
	void setConnectionTimeout(unsigned short timeout);

	void addChannel(const SAPChannelInfo &channel);
	void removeChannel(unsigned short channelId);
	QList<SAPChannelInfo> channels() const;

private:
	QSharedDataPointer<SAPServiceInfoData> data;
};

inline SAPServiceInfo::Role SAPServiceInfo::oppositeRole(Role role)
{
	switch (role) {
	case RoleProvider:
		return RoleConsumer;
	case RoleConsumer:
		return RoleProvider;
	default:
		abort();
	}
}

#endif // SAPSERVICEINFO_H
