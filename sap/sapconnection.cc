#include "sappeer.h"
#include "sapconnection.h"

SAPConnection::SAPConnection(SAPPeer *peer, const QString &profile)
    : QObject(peer), _profile(profile)
{
}

SAPPeer* SAPConnection::peer()
{
	return static_cast<SAPPeer*>(parent());
}

QString SAPConnection::profile() const
{
	return _profile;
}

SAPSocket * SAPConnection::getSocket(int channelId)
{
	return _sockets.value(channelId, 0);
}

QList<SAPSocket*> SAPConnection::sockets()
{
	return _sockets.values();
}

void SAPConnection::setSocket(int channelId, SAPSocket *socket)
{
	Q_ASSERT(!_sockets.contains(channelId));
	_sockets.insert(channelId, socket);
}
