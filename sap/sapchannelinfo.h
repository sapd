#ifndef SAPCHANNELINFO_H
#define SAPCHANNELINFO_H

#include <QtCore/QObject>
#include <QtCore/QSharedDataPointer>
#include <QtCore/QDebug>

class SAPChannelInfoData;

class SAPChannelInfo
{
	Q_GADGET

public:
	SAPChannelInfo();
	SAPChannelInfo(const SAPChannelInfo &);
	SAPChannelInfo &operator=(const SAPChannelInfo &);
	~SAPChannelInfo();

	enum PayloadType {
		PayloadNone = 0,
		PayloadBinary = 1,
		PayloadJson = 2,
		PayloadAll = 0xFF
	};

	enum QoSType {
		QoSUnrestrictedInOrder = 0,
		QoSUnrestricted = 1,
		QoSRestrictedInOrder = 2,
		QoSRestricted = 3,
		QoSReliabilityDisable = 4,
		QoSReliabilityEnable = 5
	};

	enum QoSPriority {
		QoSPriorityLow = 0,
		QoSPriorityMedium,
		QoSPriorityHigh
	};

	enum QoSDataRate {
		QoSDataRateLow = 0,
		QoSDataRateHigh
	};

	unsigned short channelId() const;
	void setChannelId(unsigned short id);

	PayloadType payloadType() const;
	void setPayloadType(PayloadType type);

	QoSType qosType() const;
	void setQoSType(QoSType type);

	QoSPriority qosPriority() const;
	void setQoSPriority(QoSPriority priority);

	QoSDataRate qosDataRate() const;
	void setQoSDataRate(QoSDataRate rate);

private:
	QSharedDataPointer<SAPChannelInfoData> data;
};

QDebug operator<<(QDebug debug, const SAPChannelInfo &info);

#endif // SAPCHANNELINFO_H
