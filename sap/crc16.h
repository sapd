#ifndef CRC16_H
#define CRC16_H

#include <stddef.h>
#include <stdint.h>

uint16_t crc16(uint16_t crc, const uint8_t *buf, size_t len);

#endif // CRC16_H
