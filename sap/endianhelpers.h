#ifndef ENDIANHELPERS_H
#define ENDIANHELPERS_H

#include <QtCore/QtEndian>

namespace
{

template<typename T>
inline T read(const QByteArray &data, int &offset)
{
	T unswapped;
	memcpy(&unswapped, &data.constData()[offset], sizeof(T)); // Unaligned access warning!
	offset += sizeof(T);
	return qFromBigEndian<T>(unswapped);
}

template<typename T>
inline void append(QByteArray &data, const T &value)
{
	T swapped = qToBigEndian<T>(value);
	data.append(reinterpret_cast<const char*>(&swapped), sizeof(T));
}

}

#endif // ENDIANHELPERS_H
