#ifndef WMSCRYPT_H
#define WMSCRYPT_H

#include <QtCore/QtGlobal>

namespace wms
{

void decrypt_block(quint8 *dst, const quint8 *src);
void decrypt_block_cbc(quint8 *dst, const quint8 *src, int len, const quint8 *iv);

}


#endif // WMSCRYPT_H
