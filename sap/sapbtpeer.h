#ifndef SAPBTPEER_H
#define SAPBTPEER_H

#include <QtCore/QObject>
#include <QtBluetooth/QBluetoothSocket>

#include "sappeer.h"

class SAPBTPeer : public SAPPeer
{
	Q_OBJECT

public:
	SAPBTPeer(SAProtocol::Role role, QBluetoothSocket *socket, QObject *parent = 0);

protected:
	void sendFrame(const QByteArray &data);

private slots:
	void handleSocketData();
	void handleSocketDisconnected();

private:
	void handleFrame(const QByteArray &data);
	void handleDataFrame(const SAProtocol::Frame &frame);
	void handleControlFrame(const SAProtocol::Frame &frame);
	void handleAuthenticationFrame(const QByteArray &data);

private:
	QBluetoothSocket *_socket;
	uint _curFrameLength;
	bool _peerDescriptionExchangeDone : 1;
	bool _authenticationDone : 1;
};

#endif // SAPBTPEER_H
