#include <QtCore/QDebug>
#include "saprotocol.h"
#include "sapmanager.h"

static SAPManager *manager = 0;

SAPManager::SAPManager(QObject *parent) :
    QObject(parent)
{
}

SAPManager * SAPManager::instance()
{
	if (!manager) {
		manager = new SAPManager;
	}
	return manager;
}

int SAPManager::registerServiceAgent(const SAPServiceInfo &service, SAPAgent *agent)
{
	const QString profile = service.profile();
	const SAPServiceInfo::Role role = service.role();

	QHash<QString, int> *profiles = profilesByRole(role);

	if (!profiles) return -1;
	if (profile.isEmpty()) return -1;
	if (profiles->contains(profile)) return -1;

	int agentId = findUnusedAgentId();
	if (agentId < 0) return -1;

	RegisteredAgent ragent;
	ragent.agentId = agentId;
	ragent.agent = agent;
	ragent.info = service;

	_agents.insert(agentId, ragent);

	profiles->insert(profile, agentId);

	return agentId;
}

void SAPManager::unregisterServiceAgent(int agentId)
{
	if (_agents.contains(agentId)) {
		const RegisteredAgent &ragent = _agents[agentId];
		const QString profile = ragent.info.profile();
		const SAPServiceInfo::Role role = ragent.info.role();

		QHash<QString, int> *profiles = profilesByRole(role);
		Q_ASSERT(profiles);

		profiles->remove(profile);
		_agents.remove(agentId);
	}
}

void SAPManager::unregisterServiceAgent(const QString &profile, SAPServiceInfo::Role role)
{
	int agentId = registeredAgentId(profile, role);

	if (agentId >= 0) {
		unregisterServiceAgent(agentId);
	}
}

int SAPManager::registeredAgentId(const QString &profile, SAPServiceInfo::Role role)
{
	QHash<QString, int> *profiles = profilesByRole(role);
	if (!profiles) return -1;

	return profiles->value(profile, -1);
}

bool SAPManager::isRegisteredAgent(int agentId) const
{
	return _agents.contains(agentId);
}

SAPAgent * SAPManager::agent(int agentId)
{
	if (!_agents.contains(agentId)) return 0;
	return _agents.value(agentId).agent;
}

SAPServiceInfo SAPManager::serviceInfo(int agentId) const
{
	return _agents.value(agentId).info;
}

QSet<QString> SAPManager::allProfiles() const
{
	return QSet<QString>::fromList(_consumerProfiles.keys())
	        + QSet<QString>::fromList(_providerProfiles.keys());
}

QSet<SAPAgent*> SAPManager::allAgents() const
{
	QSet<SAPAgent*> agents;
	foreach (const RegisteredAgent &ragent, _agents) {
		agents.insert(ragent.agent);
	}
	return agents;
}

void SAPManager::registerApplicationPackage(const RegisteredApplication &app)
{
	_pkgs.insert(app.package, app);
}

void SAPManager::registerApplicationPackage(const QString &package, const QString &name, int version, bool preinstalled)
{
	RegisteredApplication app;
	app.package = package;
	app.name = name;
	if (app.name.isEmpty()) app.name = package;
	app.version = version;
	app.preinstalled = preinstalled;
	registerApplicationPackage(app);
}

void SAPManager::unregisterApplicationPackage(const QString &package)
{
	_pkgs.remove(package);
}

QList<SAPManager::RegisteredApplication> SAPManager::allPackages() const
{
	return _pkgs.values();
}

int SAPManager::findUnusedAgentId() const
{
	if (_agents.size() > 20000) {
		qWarning() << "Ran out of agent ids!";
		return -1;
	}

	int id = 1;
	while (_agents.contains(id)) {
		id++;
	}

	return id;
}

QHash<QString, int>* SAPManager::profilesByRole(SAPServiceInfo::Role role)
{
	switch (role) {
	case SAPServiceInfo::RoleProvider:
		return &_providerProfiles;
	case SAPServiceInfo::RoleConsumer:
		return &_consumerProfiles;
	default:
		return 0;
	}
}
