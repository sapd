#ifndef SAPMANAGER_H
#define SAPMANAGER_H

#include <QtCore/QObject>
#include <QtCore/QMap>
#include <QtCore/QHash>
#include "sapserviceinfo.h"

class SAPAgent;

class SAPManager : public QObject
{
	Q_OBJECT

	explicit SAPManager(QObject *parent = 0);
	Q_DISABLE_COPY(SAPManager)

public:
	static SAPManager * instance();

	int registerServiceAgent(const SAPServiceInfo &service, SAPAgent *agent);
	void unregisterServiceAgent(int agentId);
	void unregisterServiceAgent(const QString &profile, SAPServiceInfo::Role role);

	int registeredAgentId(const QString &profile, SAPServiceInfo::Role role);

	bool isRegisteredAgent(int agentId) const;
	SAPAgent *agent(int agentId);
	SAPServiceInfo serviceInfo(int agentId) const;

	QSet<QString> allProfiles() const;
	QSet<SAPAgent*> allAgents() const;

	struct RegisteredApplication {
		QString package;
		QString name;
		int version;
		bool preinstalled;
	};

	void registerApplicationPackage(const RegisteredApplication &app);
	void registerApplicationPackage(const QString &package, const QString &name = QString(), int version = 1, bool preinstalled = false);
	void unregisterApplicationPackage(const QString &package);

	QList<RegisteredApplication> allPackages() const;

private:
	int findUnusedAgentId() const;
	QHash<QString, int>* profilesByRole(SAPServiceInfo::Role role);

private:
	struct RegisteredAgent {
		int agentId;
		SAPServiceInfo info;
		SAPAgent *agent;
	};

	QMap<int, RegisteredAgent> _agents;

	QHash<QString, int> _consumerProfiles;
	QHash<QString, int> _providerProfiles;

	QHash<QString, RegisteredApplication> _pkgs;
};

#endif // SAPMANAGER_H
