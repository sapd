#ifndef WMSPEER_H
#define WMSPEER_H

#include <QtCore/QObject>
#include <openssl/ec.h>

#include "saprotocol.h"

class WMSPeer : public QObject
{
	Q_OBJECT

public:
	WMSPeer(SAProtocol::Role role, const QString &localName, const QString &peerName, QObject *parent = 0);
	~WMSPeer();

	SAProtocol::SecurityFrame respondToServerChallenge(const SAProtocol::SecurityFrame &challenge);
	bool verifyServerResponse(const SAProtocol::SecurityFrame &challenge);

private:
	bool generateEccKey();

	QByteArray computeSharedSecret(const QByteArray &remotePubKey) const;
	QByteArray expandTemporaryKey();

	static QByteArray getExpandedPskKey(quint16 index);

	static QByteArray sha256(const QByteArray &message);

private:
	EC_KEY *_key;
	QByteArray _pubKey;
	quint32 _id;

	QString _serverName;
	QString _clientName;

	QByteArray _sharedSecret;

	quint16 _clientTmpNum;
	QByteArray _clientTmpKey;
	quint16 _serverTmpNum;
	QByteArray _serverTmpKey;
};

#endif // WMSPEER_H
