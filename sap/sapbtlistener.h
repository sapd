#ifndef SAPBTLISTENER_H
#define SAPBTLISTENER_H

#include <QtCore/QObject>
#include <QtBluetooth/QBluetoothServer>
#include <QtBluetooth/QBluetoothServiceInfo>

class SAPBTListener : public QObject
{
	Q_OBJECT
public:
	explicit SAPBTListener(QObject *parent = 0);
	~SAPBTListener();

signals:

public slots:
	void start();
	void stop();

	void nudge(const QBluetoothAddress &address);

private:

private slots:
	void acceptConnection();
    void handleNudgeConnected();
	void handleNudgeError(QBluetoothSocket::SocketError error);

private:
	QBluetoothServer *_server;
	QBluetoothServiceInfo _service;
};

#endif // SAPBTLISTENER_H
