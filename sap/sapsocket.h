#ifndef SAPSOCKET_H
#define SAPSOCKET_H

#include <QtCore/QObject>
#include <QtCore/QQueue>
#include <QtCore/QBasicTimer>

#include "sapchannelinfo.h"

class SAPConnection;
class SAPPeer;

class SAPSocket : public QObject
{
	Q_OBJECT

	SAPSocket(SAPConnection *conn, int sessionId, const SAPChannelInfo &chanInfo);

public:
	SAPPeer *peer();
	SAPConnection *connection();

	SAPChannelInfo channelInfo() const;

	bool isOpen() const;

	bool messageAvailable() const;
	QByteArray receive();
	bool send(const QByteArray &data);

signals:
	void connected();
	void disconnected();
	void messageReceived();

protected:
	void setOpen(bool open);
	void acceptIncomingData(const QByteArray &data);
	void acceptIncomingControl(const QByteArray &data);

	int sessionId() const;

	virtual void timerEvent(QTimerEvent *event) override;

private:
	bool isReliable() const;
	bool supportsFragmentation() const;

	void sendBlockAck(int seqNum);
	void sendPacket(int seqNum, const QByteArray &data);

	void handleBlockAck(int seqNum);
	void sendPacketsFromQueue();

private:
	const int _sessionId;
	const SAPChannelInfo _info;
	bool _open;
	QQueue<QByteArray> _in;
	QQueue<QByteArray> _out;
	QBasicTimer _ackTimer;
	QBasicTimer _resendTimer;

	/** Last acknowledged sent message. */
	quint16 _outLastAck;
	quint16 _outFlyingPkts;

	/** Next expected incoming sequence number */
	quint16 _inLastSeqNum;
	/** Last acknowledged sequence number */
	quint16 _inLastAck;

	friend class SAPPeer;
};

#endif // SAPSOCKET_H
