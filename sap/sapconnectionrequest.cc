#include "sappeer.h"
#include "sapconnection.h"
#include "sapconnectionrequest.h"

SAPConnectionRequest::SAPConnectionRequest(SAPConnection *conn, int initiatorId, int acceptorId)
    : _conn(conn), _initiator(initiatorId), _acceptor(acceptorId)
{
}

SAPConnection *SAPConnectionRequest::connection()
{
	return _conn;
}

SAPPeer * SAPConnectionRequest::peer()
{
	return connection()->peer();
}

void SAPConnectionRequest::accept()
{
	peer()->acceptServiceConnection(this, 0);
	delete this;
}

void SAPConnectionRequest::reject(int reason)
{
	peer()->acceptServiceConnection(this, reason);
	delete this;
}

int SAPConnectionRequest::initiatorId() const
{
	return _initiator;
}

int SAPConnectionRequest::acceptorId() const
{
	return _acceptor;
}
