#include <QtCore/QMap>
#include <QtCore/QSharedData>

#include "sapserviceinfo.h"
#include "sapchannelinfo.h"


class SAPServiceInfoData : public QSharedData {
public:
	QString profile;
	QString friendlyName;
	SAPServiceInfo::Role role;
	unsigned short version;
	unsigned short connectionTimeout;

	QMap<int, SAPChannelInfo> channels;
};

SAPServiceInfo::SAPServiceInfo() : data(new SAPServiceInfoData)
{
}

SAPServiceInfo::SAPServiceInfo(const SAPServiceInfo &rhs) : data(rhs.data)
{
}

SAPServiceInfo &SAPServiceInfo::operator=(const SAPServiceInfo &rhs)
{
	if (this != &rhs)
		data.operator=(rhs.data);
	return *this;
}

SAPServiceInfo::~SAPServiceInfo()
{
}

QString SAPServiceInfo::profile() const
{
	return data->profile;
}

void SAPServiceInfo::setProfile(const QString &profile)
{
	data->profile = profile;
}

QString SAPServiceInfo::friendlyName() const
{
	return data->friendlyName;
}

void SAPServiceInfo::setFriendlyName(const QString &name)
{
	data->friendlyName = name;
}

SAPServiceInfo::Role SAPServiceInfo::role() const
{
	return data->role;
}

void SAPServiceInfo::setRole(SAPServiceInfo::Role role)
{
	data->role = role;
}

unsigned short SAPServiceInfo::version() const
{
	return data->version;
}

void SAPServiceInfo::setVersion(unsigned short version)
{
	data->version = version;
}

void SAPServiceInfo::setVersion(unsigned char maj, unsigned char min)
{
	setVersion((maj << 8) | (min & 0xFF));
}

unsigned short SAPServiceInfo::connectionTimeout() const
{
	return data->connectionTimeout;
}

void SAPServiceInfo::setConnectionTimeout(unsigned short timeout)
{
	data->connectionTimeout = timeout;
}

void SAPServiceInfo::addChannel(const SAPChannelInfo &channel)
{
	data->channels.insert(channel.channelId(), channel);
}

void SAPServiceInfo::removeChannel(unsigned short channelId)
{
	data->channels.remove(channelId);
}

QList<SAPChannelInfo> SAPServiceInfo::channels() const
{
	return data->channels.values();
}
