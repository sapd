#ifndef SAAGENT_H
#define SAAGENT_H

#include <QtCore/QString>

class SAPSocket;
class SAPPeer;
class SAPConnectionRequest;

class SAPAgent
{
public:
	virtual void peerFound(SAPPeer *peer) = 0;
	virtual void requestConnection(SAPConnectionRequest *request) = 0;
};

#endif // SAAGENT_H
