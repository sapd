#ifndef WEBPROXYAGENT_H
#define WEBPROXYAGENT_H

#include <QtCore/QObject>
#include <sap/sappeer.h>
#include <sap/sapmanager.h>
#include <sap/sapagent.h>

class WebProxyAgent : public QObject, public SAPAgent
{
	Q_OBJECT

	explicit WebProxyAgent(QObject *parent = 0);

public:
	static WebProxyAgent * instance();
	static void registerServices(SAPManager *manager);

	void peerFound(SAPPeer *peer);
	void requestConnection(SAPConnectionRequest *request);

private:
	SAPPeer *_peer;
	SAPSocket *_socket;
};

#endif // WEBPROXYAGENT_H
