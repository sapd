#include <sap/sapsocket.h>
#include <sap/sapconnectionrequest.h>
#include <sap/sapserviceinfo.h>
#include <sap/sapchannelinfo.h>
#include "musicconn.h"
#include "musicagent.h"

static MusicAgent *agent = 0;
static const QLatin1String music_profile("/system/music");

MusicAgent::MusicAgent(QObject *parent)
	: QObject(parent), _peer(0), _socket(0)
{
}

MusicAgent* MusicAgent::instance()
{
	if (!agent) {
		agent = new MusicAgent;
	}
	return agent;
}

void MusicAgent::peerFound(SAPPeer *peer)
{
	Q_UNUSED(peer);
}

void MusicAgent::requestConnection(SAPConnectionRequest *request)
{
	qDebug() << "MusicAgent request connection from" << request->peer()->peerName();
	SAPConnection *conn = request->connection();
	new MusicConn(conn, this);
	request->accept();
}

void MusicAgent::registerServices(SAPManager *manager)
{
	SAPServiceInfo service;
	SAPChannelInfo channel;

	service.setProfile(music_profile);
	service.setFriendlyName("Media controller");
	service.setRole(SAPServiceInfo::RoleProvider);
	service.setVersion(1, 0);
	service.setConnectionTimeout(0);

	channel.setChannelId(100);
	channel.setPayloadType(SAPChannelInfo::PayloadNone);
	channel.setQoSType(SAPChannelInfo::QoSReliabilityDisable);
	channel.setQoSDataRate(SAPChannelInfo::QoSDataRateLow);
	channel.setQoSPriority(SAPChannelInfo::QoSPriorityLow);
	service.addChannel(channel);

	manager->registerServiceAgent(service, instance());
}
