#ifndef HOSTMANAGERPEER_H
#define HOSTMANAGERPEER_H

#include <sap/sapconnection.h>
#include <sap/sapsocket.h>

class HostManagerConn : public QObject
{
	Q_OBJECT

public:
	HostManagerConn(SAPConnection *conn, QObject *parent = 0);

protected:
	struct AppInfo {
		QString name;
		QString packageName;
		QString version;
		bool preloaded;
		bool isAppWidget;

		QStringList requiredPackages;
		QStringList requiringPackages;
		bool installed;
	};

	struct DeviceInfo {
		QString deviceId;
		QString deviceName;
		QString devicePlatform;
		QString devicePlatformVersion;
		QString deviceType;
		QString modelNumber;
		QString swVersion;

		QList<AppInfo> apps;

		bool telephony;
		bool messaging;
		bool tablet;
		bool autolock;
		bool smartrelay;
		bool safetyassistence;
		QString vendor;
	};

	static DeviceInfo parseDeviceInfo(const QString &xmlData);

private:
	void sendMessage(const QJsonObject &obj);

	void handleMessage(const QJsonObject &obj);

	void performTimeSync();

	QString generateHostXml();

private slots:
	void handleConnected();
	void handleMessageReceived();

private:
	SAPConnection *_conn;
	SAPSocket *_socket;
};

#endif // HOSTMANAGERPEER_H
