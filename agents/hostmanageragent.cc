#include <sap/sapsocket.h>
#include <sap/sapconnectionrequest.h>
#include <sap/sapserviceinfo.h>
#include <sap/sapchannelinfo.h>
#include "hostmanagerconn.h"
#include "hostmanageragent.h"

static HostManagerAgent *agent = 0;
static const QLatin1String hostmanager_profile("/system/hostmanager");

HostManagerAgent::HostManagerAgent(QObject *parent)
    : QObject(parent), _peer(0), _socket(0)
{
}

HostManagerAgent* HostManagerAgent::instance()
{
	if (!agent) {
		agent = new HostManagerAgent;
	}
	return agent;
}

void HostManagerAgent::peerFound(SAPPeer *peer)
{
	Q_UNUSED(peer);
}

void HostManagerAgent::requestConnection(SAPConnectionRequest *request)
{
	qDebug() << "Host manager request connection from" << request->peer()->peerName();
	SAPConnection *conn = request->connection();
	new HostManagerConn(conn, this);

	request->accept();
}

void HostManagerAgent::registerServices(SAPManager *manager)
{
	SAPServiceInfo service;
	SAPChannelInfo channel;

	service.setProfile(hostmanager_profile);
	service.setFriendlyName("HostManager");
	service.setRole(SAPServiceInfo::RoleProvider);
	service.setVersion(1, 0);
	service.setConnectionTimeout(0);

	channel.setChannelId(103);
	channel.setPayloadType(SAPChannelInfo::PayloadJson);
	channel.setQoSType(SAPChannelInfo::QoSUnrestrictedInOrder);
	channel.setQoSDataRate(SAPChannelInfo::QoSDataRateLow);
	channel.setQoSPriority(SAPChannelInfo::QoSPriorityHigh);
	service.addChannel(channel);

	manager->registerServiceAgent(service, instance());
}
