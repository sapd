#ifndef WEBPROXYCONN_H
#define WEBPROXYCONN_H

#include <sap/sapconnection.h>
#include <sap/sapsocket.h>

class WebProxyTrans;

class WebProxyConn : public QObject
{
	Q_OBJECT

public:
	WebProxyConn(SAPConnection *conn, QObject *parent = 0);

protected:
	enum MessageType {
		MessageRequest = 1,
		MessageResponse = 2,
		MessageError = 3,
		MessageAbort = 4
	};

	struct Message {
		quint8 command; // Seems to be always 1
		quint8 subCommand; // Seems to be always 1
		MessageType type;
		quint8 transactionId; // Monotonically increasing
		QByteArray payload;
	};

	static Message unpackMessage(const QByteArray &data);
	static QByteArray packMessage(const Message &msg);

	struct RequestHeader {
		/** Whether this is a CONNECT request, i.e. tunnel. */
		bool connect;
		QString host;
		int port;
	};

	static RequestHeader parseRequestHeader(const QByteArray &req);
	static QByteArray removeHeaders(const QByteArray &req);

	void sendMessage(const Message &msg);

	void handleRequest(const Message &msg);
	void handleAbort(const Message &msg);

private slots:
	void handleMessageReceived();
	void handleTransDataReceived(const QByteArray &data);
	void handleTransDisconnected();

private:
	SAPConnection *_conn;
	SAPSocket *_in;
	SAPSocket *_out;
	QMap<int, WebProxyTrans*> _trans;
};

#endif // WEBPROXYCONN_H
