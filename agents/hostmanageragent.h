#ifndef HOSTMANAGER_H
#define HOSTMANAGER_H

#include <QtCore/QObject>
#include <sap/sappeer.h>
#include <sap/sapmanager.h>
#include <sap/sapagent.h>

class HostManagerAgent : public QObject, public SAPAgent
{
	Q_OBJECT

	explicit HostManagerAgent(QObject *parent = 0);

public:
	static HostManagerAgent * instance();
	static void registerServices(SAPManager *manager);

	void peerFound(SAPPeer *peer);
	void requestConnection(SAPConnectionRequest *request);

private:
	SAPPeer *_peer;
	SAPSocket *_socket;
};

#endif // HOSTMANAGER_H
