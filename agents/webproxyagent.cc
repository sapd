#include <sap/sapsocket.h>
#include <sap/sapconnectionrequest.h>
#include <sap/sapserviceinfo.h>
#include <sap/sapchannelinfo.h>
#include "webproxyconn.h"
#include "webproxyagent.h"

static WebProxyAgent *agent = 0;
static const QLatin1String webproxy_profile("/system/webproxy");

WebProxyAgent::WebProxyAgent(QObject *parent)
	: QObject(parent), _peer(0), _socket(0)
{
}

WebProxyAgent* WebProxyAgent::instance()
{
	if (!agent) {
		agent = new WebProxyAgent;
	}
	return agent;
}

void WebProxyAgent::peerFound(SAPPeer *peer)
{
	Q_UNUSED(peer);
}

void WebProxyAgent::requestConnection(SAPConnectionRequest *request)
{
	qDebug() << "WebProxy request connection from" << request->peer()->peerName();
	SAPConnection *conn = request->connection();
	new WebProxyConn(conn, this);

	request->accept();
}

void WebProxyAgent::registerServices(SAPManager *manager)
{
	SAPServiceInfo service;
	SAPChannelInfo channel;

	service.setProfile(webproxy_profile);
	service.setFriendlyName("WebProxy");
	service.setRole(SAPServiceInfo::RoleProvider);
	service.setVersion(2, 0);
	service.setConnectionTimeout(0);

	channel.setChannelId(501);
	channel.setPayloadType(SAPChannelInfo::PayloadBinary);
	channel.setQoSType(SAPChannelInfo::QoSReliabilityEnable);
	channel.setQoSDataRate(SAPChannelInfo::QoSDataRateLow);
	channel.setQoSPriority(SAPChannelInfo::QoSPriorityLow);
	service.addChannel(channel);

	channel.setChannelId(502);
	channel.setPayloadType(SAPChannelInfo::PayloadBinary);
	channel.setQoSType(SAPChannelInfo::QoSReliabilityEnable);
	channel.setQoSDataRate(SAPChannelInfo::QoSDataRateLow);
	channel.setQoSPriority(SAPChannelInfo::QoSPriorityLow);
	service.addChannel(channel);

	manager->registerServiceAgent(service, instance());
}
