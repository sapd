#include <sap/sapsocket.h>
#include <sap/sapconnectionrequest.h>
#include <sap/sapserviceinfo.h>
#include <sap/sapchannelinfo.h>
#include "notificationconn.h"
#include "notificationagent.h"

static NotificationAgent *agent = 0;
static const QLatin1String notification_profile("/system/NotificationService");

NotificationAgent::NotificationAgent(QObject *parent)
    : QObject(parent), _peer(0), _socket(0)
{
}

NotificationAgent* NotificationAgent::instance()
{
	if (!agent) {
		agent = new NotificationAgent;
	}
	return agent;
}

void NotificationAgent::peerFound(SAPPeer *peer)
{
	Q_UNUSED(peer);
}

void NotificationAgent::requestConnection(SAPConnectionRequest *request)
{
	qDebug() << "Notification request connection from" << request->peer()->peerName();
	SAPConnection *conn = request->connection();
	new NotificationConn(conn, this);

	request->accept();
}

void NotificationAgent::registerServices(SAPManager *manager)
{
	SAPServiceInfo service;
	SAPChannelInfo channel;

	service.setProfile(notification_profile);
	service.setFriendlyName("Notification");
	service.setRole(SAPServiceInfo::RoleProvider);
	service.setVersion(1, 0);
	service.setConnectionTimeout(0);

	channel.setChannelId(104);
	channel.setPayloadType(SAPChannelInfo::PayloadBinary);
	channel.setQoSType(SAPChannelInfo::QoSReliabilityDisable);
	channel.setQoSDataRate(SAPChannelInfo::QoSDataRateLow);
	channel.setQoSPriority(SAPChannelInfo::QoSPriorityLow);
	service.addChannel(channel);

	manager->registerServiceAgent(service, instance());
}
