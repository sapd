#ifndef MUSICAGENT_H
#define MUSICAGENT_H

#include <QtCore/QObject>
#include <sap/sappeer.h>
#include <sap/sapmanager.h>
#include <sap/sapagent.h>

class MusicAgent : public QObject, public SAPAgent
{
public:
	explicit MusicAgent(QObject *parent = 0);

public:
	static MusicAgent * instance();
	static void registerServices(SAPManager *manager);

	void peerFound(SAPPeer *peer);
	void requestConnection(SAPConnectionRequest *request);

private:
	SAPPeer *_peer;
	SAPSocket *_socket;
};

#endif // MUSICAGENT_H
