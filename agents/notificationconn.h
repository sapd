#ifndef NOTIFICATIONCONN_H
#define NOTIFICATIONCONN_H

#include <QtCore/QDateTime>
#include <QtCore/QHash>
#include <sap/sapconnection.h>
#include <sap/sapsocket.h>

#if SAILFISH
namespace watchfish
{
class Notification;
class NotificationMonitor;
}
#endif

class NotificationConn : public QObject
{
	Q_OBJECT

public:
	NotificationConn(SAPConnection *conn, QObject *parent = 0);

protected:
	enum NotificationType {
		NotificationPopup = 0
	};

	enum NotificationAttributeType {
		NotificationPackageName = 0,
		NotificationCount = 1,
		NotificationTitle = 2,
		NotificationTime = 3,
		NotificationSender = 4,
		NotificationBody = 5,
		NotificationThumbnail = 6,
		NotificationApplicationName = 7,
		NotificationOpenInWatch = 9,
		NotificationOpenInHost = 10,
		NotificationId = 11
	};

	struct Notification {
		Notification();

		NotificationType type;
		int sequenceNumber;
		bool urgent;
		int applicationId;
		int category;
		QString packageName;
		int count;
		QString title;
		QDateTime time;
		int sender;
		QString body;
		QByteArray thumbnail;
		QString applicationName;
		//bool openInWatch;
		//bool openInHost;
		int notificationId;
	};

	QByteArray packNotification(const Notification &n);
	void sendNotification(const Notification &n);

private slots:
	void handleConnected();
	void handleMessageReceived();

#if SAILFISH
	void handleNotification(watchfish::Notification *n);
#endif

	void performTest();

private:
	SAPConnection *_conn;
	SAPSocket *_socket;
	QHash<QString, short> _knownSenders;
	int _lastSeqNumber;
	int _lastNotifId;
};

#endif // NOTIFICATIONCONN_H
