#include <QtCore/QDebug>
#include <QtCore/QUrl>

#include "webproxyconn.h"
#include "webproxytrans.h"

static const QByteArray connectResponse("HTTP/1.1 200 Connection established\r\n\r\n");

WebProxyTrans::WebProxyTrans(int transactionId, bool tunnel, const QString &host, int port, WebProxyConn *conn)
	: QObject(conn), _transactionId(transactionId), _tunnel(tunnel), _socket(new QTcpSocket(this))
{
	connect(_socket, SIGNAL(connected()), this, SLOT(handleSocketConnected()));
	connect(_socket, SIGNAL(bytesWritten(qint64)), this, SLOT(handleSocketDataWritten(qint64)));
	connect(_socket, SIGNAL(readyRead()), this, SLOT(handleSocketDataReady()));
	connect(_socket, SIGNAL(disconnected()), this, SLOT(handleSocketDisconnected()));

	_socket->connectToHost(host, port);
}

int WebProxyTrans::transactionId() const
{
	return _transactionId;
}

void WebProxyTrans::write(const QByteArray &data)
{
	if (!_outBuf.isEmpty() || !_socket->isValid()) {
		_outBuf.append(data);
	} else if (_socket->isValid()) {
		// Write directly to socket
		_socket->write(data);
	}
}

void WebProxyTrans::handleSocketConnected()
{
	qDebug() << "Transaction" << _transactionId << "Connected";
	if (_tunnel) {
		emit dataReceived(connectResponse);
	}
	writeFromBuffer();
}

void WebProxyTrans::handleSocketDataWritten(qint64 written)
{
	Q_UNUSED(written);
	writeFromBuffer();
}

void WebProxyTrans::handleSocketDataReady()
{
	emit dataReceived(_socket->readAll());
}

void WebProxyTrans::handleSocketDisconnected()
{
	qDebug() << "Transaction" << _transactionId << "Disconnected";
	emit disconnected();
}

WebProxyConn* WebProxyTrans::conn()
{
	return static_cast<WebProxyConn*>(parent());
}

void WebProxyTrans::writeFromBuffer()
{
	if (!_outBuf.isEmpty()) {
		qint64 written = _socket->write(_outBuf);
		if (written > 0) {
			_outBuf.remove(0, written);
		}
	}
}
