#ifndef MUSICCONN_H
#define MUSICCONN_H

#include <sap/sapconnection.h>
#include <sap/sapsocket.h>

class MusicConn : public QObject
{
	Q_OBJECT

public:
	MusicConn(SAPConnection *conn, QObject *parent = 0);

protected:
	static QString encodeAlbumArt(const QString &albumArt);

private:
	void sendMessage(const QJsonObject &msg);
	void handleMessage(const QJsonObject &msg);

	void sendResponse(const QString &id, const QString &result, int reason);
	void sendMediaChangedInd();

private slots:
	void handleConnected();
	void handleMessageReceived();
	void handleMetadataChanged();

private:
	SAPConnection *_conn;
	SAPSocket *_socket;
};

#endif // MUSICCONN_H
