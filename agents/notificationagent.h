#ifndef NOTIFICATIONAGENT_H
#define NOTIFICATIONAGENT_H

#include <QtCore/QObject>
#include <sap/sappeer.h>
#include <sap/sapmanager.h>
#include <sap/sapagent.h>

class NotificationAgent : public QObject, public SAPAgent
{
	Q_OBJECT

	explicit NotificationAgent(QObject *parent = 0);

public:
	static NotificationAgent * instance();
	static void registerServices(SAPManager *manager);

	void peerFound(SAPPeer *peer);
	void requestConnection(SAPConnectionRequest *request);

private:
	SAPPeer *_peer;
	SAPSocket *_socket;
};

#endif // NOTIFICATIONAGENT_H
