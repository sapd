#ifndef WEBPROXYTRANS_H
#define WEBPROXYTRANS_H

#include <QtCore/QIODevice>
#include <QtNetwork/QTcpSocket>

class WebProxyConn;

class WebProxyTrans : public QObject
{
	Q_OBJECT
public:
	explicit WebProxyTrans(int transactionId, bool tunnel, const QString &host, int port, WebProxyConn *conn);

	int transactionId() const;

	void write(const QByteArray &data);

signals:
	void connected();
	void dataReceived(const QByteArray &data);
	void error();
	void disconnected();

private slots:
	void handleSocketConnected();
	void handleSocketDataWritten(qint64 written);
	void handleSocketDataReady();
	void handleSocketDisconnected();

private:
	WebProxyConn* conn();
	void writeFromBuffer();

private:
	int _transactionId;
	bool _tunnel;
	QTcpSocket *_socket;
	QByteArray _outBuf;
};

#endif // WEBPROXYTRANS_H
